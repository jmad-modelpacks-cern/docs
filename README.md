## JMad Modelpacks Documentation

This repository contains explanations how to use the JMad Modelpacks for developing and releasing JMad models.


* An overview of the proposed structure can be found in [proposed-structure.md](proposed-structure.md).
* How to manage model variants by the use of releases, tags and branches is described in [model-variants.md](model-variants.md).
* A first glance on how to use the jmad model definition DSL can be found in [using-jmad-modeldefs-dsl.md](using-jmad-modeldefs-dsl.md).