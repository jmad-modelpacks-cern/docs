# JMad Modeldefs DSL

While in principle, the jmad model xml files could be created by hand, it is highly recommended to use the java internal DSL to do so. This way, one can profit from code completion of IDEs and the generated xml files are automatically correct.

As a example, the [linac4 model package](https://gitlab.cern.ch/jmad-modelpacks-cern/jmad-modelpack-linac4/tree/master/src/test/org/jmad/modelpack/linac4/modeldef/) is already created in that way.

## Project structure and definition file

To use the DSL, it is required to stick to a java-project like structure in the model package (similar as it was the case when jmad was using jars to distribute the models). So the minimum recommended layout looks something like this:
```
src/java/
src/test/
product.xml
```

In the `product.xml` at least the following dependencies have to be declared:
```
<dep product="jmad-core-pro" version="PRO" />
```

Additionally, we recommend to also include the `junit` library, to be able to create tests for the models:
```
<dep product="junit" local="true" version="PRO" />
```

We propose to place the java file, which uses the DSL, somewhere below the `src/test` folder. It has to be a java class which inherits from `JMadModelDefinitionDslSupport`. For example:

```
public class Linac4ModelFactory extends JMadModelDefinitionDslSupport {{
    /* definition goes here */
}}
```
NOTE: The two `{{` in the beginning and the corresponding `}}` are correct - This allows to use methods from JMadModelDefinitionDslSupport as shown in the following example ;-).

An example model definition looks then somehow like this:
```
public class Linac4ModelFactory extends JMadModelDefinitionDslSupport {{
    name("LINAC4");

    offsets(o -> {
        o.repository("2018");
    });

    init(i -> {
        i.call("aperture/l4t.dbx");
        i.call("elements/l4t.ele");
        i.call("sequence/l4t.seq");
        i.call("beam/beam.beamx");
        i.call("strength/l4t.str");
    });

    sequence("l4t").isDefault().isDefinedAs(s -> {
        s.range("ALL").isDefault().isDefinedAs(r -> {
            r.twiss(t -> {
                t.betx(5.53);
                t.alfx(1.74);
                t.bety(11.0);
                t.alfy(-3.0);
                t.calcAtCenter();
            });
        });
    });

    optics("l4t_2018").isDefault().isDefinedAs(o -> {
        o.call("strength/l4t.str").parseAs(STRENGTHS);
    });
}}
```

# Generating the model-definition xml

Before comitting the project, the jmad model definition xml files (ending in `*.jmd.xml`) have to be generated. To do so, an automatic tool is included in jmad. To use it, we propose to add a simple main method to the project (also below the `src/test` folder):

```
import static cern.accsoft.steering.jmad.tools.modeldefs.creating.ModelDefinitionCreator.scanDefault;

public class Linac4ModelDefCreator {

    public static void main(String[] args) {
        scanDefault().and().writeTo("src/java/org/jmad/modelpack/linac4/modeldef/defs");
    }

}
```
Hereby, the path in the `writeTo(..)` method describes where the model definitions shall be written to. Note this has to be the path, of which the `repdata` and `resdata` folders are direct subfolders. Each time this main is ran, the actual project is scanned and the all classes which extend `JMadModelFactory` are executed and the created model definitions are stored into xml files.

# Keeping the model-package clean and concise

To avoid keeping unused files forever in the modelpackage, another additional tool can be used to clean the model package. For this we propose to have another short main method in the project:
```
import static cern.accsoft.steering.jmad.tools.modeldefs.cleaning.ModelPackageCleaner.cleanUnusedBelow;

public class Linac4ModelDefCleaner {

    public static void main(String[] args) {
        cleanUnusedBelow("src/java");
    }

}

```
Whenever this main method is run, all files below the given path ("src/java"), which are not referenced by any model definition are deleted. NOTE: Choose this path carefully! For this reason, we also propose to keep all the java files and other files which shall be kept, even if they are not part of the model definition, in the "src/test" path.