# Treating model variants

There is an understandable need for keeping old models, e.g. to be able to analyze old configuration and/or runs even years later. However, we **highly recommend to NOT have different variants within one modelpackage**.

Instead we propose to use git branches, releases and tags. This way, the model name can stay constant over years (which makes it convenient for software that need to use the model), and the changes and variants are properly tracked. When the model variants are listed in the [JMad Modelpack GUI](https://github.com/jmad/jmad-modelpack-fx), they are listed in the following order:
1. Releases (highest one first) 
2. Tags (highest one first) 
3. Branches (highest one first) 

Therefore, when releases are consequently used, the topmost variant will always be the latest release.

## Releases

A release of a model package can be created from the Gitlab web GUI[^gitlab-release] in the following way:

1. In the project menu select `Repository->Tags` and press on the button `New Tag`
2. Add a tag name and a release Note (!!!) - **If no release note is added, then only the tag is created, but it is not treated as a release by Gitlab**: 
* We **HIGHLY RECOMMEND** to use the format "vYYYY.X" for release tags. For example: **`v2018.1`**, **`v2018.2`**, ... This way, the year is clearly visible from the release and the last digit is increased steadily.

## Tags

Tags are standard Git tags [^git-tags]

* Tags can be used basically freely, to mark certain states of the model package. (They also can again be deleted later)
* Tags are created in the same way than a release, just that no release note is entered.

## Branches

Branches are standard Git branches [^git-branches]

* **master:** We propose to have the `master` branch always up to date with the latest commit, so basically always ready for a new release. If you are doing work, which potentially breaks things (for longer), then we highly recommend to create your own branch for testing.
* **Switching runs/years:** In case it is required to still be able to change things for old years (e.g. for later analysis or simulations), it could be useful to have a branch per run or per year, as there might appear major changes in between. For example, before we will delete the old models from the master branch, we would create a new branch (or tag) something like "before-2018-migration".

[^gitlab-release]: https://docs.gitlab.com/ee/workflow/releases.html
[^git-tags]: https://git-scm.com/book/en/v2/Git-Basics-Tagging
[^git-branches]: https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell
